#!/usr/bin/env python
"""
    This script is supposed to clean unneeded RAW images. Nikon D3500 camera is set up to save one .JPG and
    one .NEF file for every shot. When the user manually looks through all JPG images created by camera
    and removes those which are not needed, there remain corresponding .NEF images.
    This script searches for the .NEF images which do not have corresponding .JPG with the same name
    and than removes found .NEF images.
"""

import argparse
import os

IMAGE_EXT = {
    'jpg': ('.jpg', '.jpeg'),
    'raw': ('.nef', ),
}


def get_all_files(path_list):
    """
    Return a dict {'raw':{base_name: extension}, 'jpg': {base_name: extension}
    for all the files in all the paths in path_list
    """

    jpg_files = []
    raw_files = []
    for path in path_list:
        for root_dir, dirs, files in os.walk(path):
            for file_name in files:
                name, extension = os.path.splitext(os.path.join(root_dir, file_name))
                if extension.lower() in IMAGE_EXT['jpg']:
                    jpg_files.append((name, extension))
                if extension.lower() in IMAGE_EXT['raw']:
                    raw_files.append((name, extension))
    return {'jpg': dict(jpg_files), 'raw': dict(raw_files)}


def remove_raw_files(files, dry_run=None):
    """ Return a list of .NEF files which do not have corresponding .JPG file with the same name """

    dry_run_message = 'Dry run: ' if dry_run else ''
    for file_name in sorted(files['raw']):
        if file_name in files['jpg']:
            raw_file = ''.join([file_name, files['raw'][file_name]])
            print(
                '{}{} exists => removing {}'
                .format(dry_run_message, ''.join([file_name, files['jpg'][file_name]]), raw_file)
            )
            if not dry_run and raw_file[:-3] not in ('jpg', 'JPG'):
                os.remove(raw_file)


def main():
    parser = argparse.ArgumentParser(
        description='This script is supposed to clean unneeded RAW images. Nikon D3500 camera is set up to save'
                    ' one .JPG and one .NEF file for every shot. Than the user manually looks through all the'
                    ' JPG images created by camera and removes those which are not needed,'
                    ' there remain corresponding .NEF images. This script searches for the .NEF images'
                    ' which do not have corresponding .JPG with the same name and than removes found .NEF images.'
    )
    parser.add_argument(
        'path_list',
        nargs='+',
        help='List path where to search for .NEF images to delete',
    )
    parser.add_argument(
        '-dr', '--dry-run',
        action='store_true',
        help='Do not remove files, just print which files will be removed',
    )
    args = parser.parse_args()
    files = get_all_files(args.path_list)
    remove_raw_files(files, args.dry_run)


if __name__ == '__main__':
    main()
